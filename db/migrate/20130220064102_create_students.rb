class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :fullname
      t.string :place_date_birth
      t.text :address
      t.string :graduated_from
      t.string :year_graduated

      t.timestamps
    end
  end
end
