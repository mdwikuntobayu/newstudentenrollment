class StudentsController < ApplicationController
  def index
    @students = Student.all
    if !@students
  		render :text => "No Student Enrolled yet"
  	end
  end

  def new
  	@student = Student.new
  end
  def create
  	@student = Student.new(params[:student])
  	if @student.save
  		redirect_to(students_path)
  	else
			render "new"
  	end
  end

  def show
    @student = Student.find(params[:id])
  end

  def edit
    @student = Student.find(params[:id])
  end
  def update
    @student = Student.find(params[:id])
  	if @student.update_attributes(params[:student])
  		render "show"
  	else
  		render "edit"
  	end
  end

  def destroy
  	@student = Student.find(params[:id])
  	if @student.destroy
  		redirect_to(students_path)
  	else
  		render "show"
  	end
  end
end
