class Student < ActiveRecord::Base
  attr_accessible :address, :fullname, :graduated_from, :place_date_birth, :year_graduated

  validates :fullname, :presence => true
  validates :place_date_birth, :presence => true
  validates :address, :presence => true
  validates :graduated_from, :presence => true
  validates :year_graduated, :presence => true, :numericality => { :greater_than => 2000, :less_than_or_equal_to => Date.today.year }

end
